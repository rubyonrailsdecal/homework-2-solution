class CitiesController < ApplicationController

  def view
    all_cities = City.all
    city_param = params[:city]
    if city_param and all_cities[city_param.to_sym]
      @cities = [all_cities[city_param.to_sym]]
    else
      @cities = all_cities.keys.map { |k| all_cities[k]  }
    end
  end

  def new # GET route
  end

  def create # POST route
    if params[:name] and params[:landmark] and params[:population]
      if !City.all[params[:name].to_sym]
        c = City.new params
        c.save
      end
    end
    redirect_to :action => 'view'
  end

  def update # Using the same route for both!
    if params[:name]
      c = City.all[params[:name].to_sym]
      if c
        c.update(params)
      end
      redirect_to :action => 'view'
    end
  end


end
