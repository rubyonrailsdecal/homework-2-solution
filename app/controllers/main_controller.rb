require_relative "../services/weather_service"

class MainController < ApplicationController

  def index
    city_name = params[:city]
    @cities = City.all.keys.map { |k| City.all[k]  }
    if city_name
      @c = City.new name: city_name, landmark: "none", population: 0
      @c.save
      @w = @c.weather
    end
  	if @w
      # Bonus: Converting Kelvin to Fahrenheit
  		@temperature = (9 / 5) * (@w[:temperature] - 273) + 32
  	end
  end
end
